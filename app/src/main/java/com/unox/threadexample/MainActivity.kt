package com.unox.threadexample

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.annotation.MainThread
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Executors
import kotlin.random.Random


val imagesUrls = listOf(
    "https://banner2.kisspng.com/20171202/e3e/fruit-free-png-image-5a22fd776d6943.7240975215122425514482.jpg",
    "http://www.pngmart.com/files/5/Red-Apple-PNG-Photos.png",
    "https://www.clipartmax.com/png/middle/66-661522_rip-banana-fruit-transparent-background-png-image-banana-png.png",
    "https://banner2.kisspng.com/20171127/d9f/fruit-basket-png-vector-clipart-image-5a1c01ee7dd394.8919843915117849425154.jpg"
)

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            val index = Random.nextInt(imagesUrls.size)
            DownloadManager.downloadImage(imagesUrls[index])
        }


        DownloadManager.setDownloadListener(object : DownloadManager.onDownloadListener {
            override fun onDownloadComplete(bitmap: Bitmap) {
                progressBar.visibility = View.GONE
                imageView.setImageBitmap(bitmap)
            }

            override fun onDownloadFailed() {
                progressBar.visibility = View.GONE
                imageView.setImageResource(R.drawable.ic_image_black_24dp)
            }

            override fun onDownloadInitiated() {
                progressBar.visibility = View.VISIBLE
            }
        })


    }

    override fun onStop() {

        super.onStop()
    }
}


object DownloadManager {
    private var isDownloading = false
    private val DOWNLOAD_COMPLETE_CODE = 1
    private val DOWNLOAD_FAILED_CODE = 2
    private val DOWNLOAD_INITIATED_CODE = 3

    private var listener: DownloadManager.onDownloadListener? = null

    private val executorService = Executors.newFixedThreadPool(3)

    /**
     * Handler che gira sul loop dell'interfaccia grafica
     */
    var handler = Handler(Looper.getMainLooper()) {
        when(it.what) {
            DOWNLOAD_INITIATED_CODE -> {
                // 1. Informare il listener del download iniziato
                listener?.onDownloadInitiated()
            }
            DOWNLOAD_COMPLETE_CODE -> {
                // 1. Resettare isDownloading
                // 2. Informare il listener del download completato
                isDownloading = false
                listener?.onDownloadComplete(it.obj as Bitmap)
            }
            DOWNLOAD_FAILED_CODE -> {
                // 1. Resettare isDownloading
                // 2. Informare il listener del fallimento
                isDownloading = false
                listener?.onDownloadFailed()
            }
        }

        true

    }


    @MainThread
    fun downloadImage(url: String) {
        if (isDownloading)
            return

        isDownloading = true
        //executorService.execute(DownloadRunnable(url))
        executorService.execute()
        Thread(DownloadRunnable(url)).start()
    }


    fun setDownloadListener(listener: onDownloadListener?) {
        DownloadManager.listener = listener
    }


    interface onDownloadListener {
        fun onDownloadInitiated()
        fun onDownloadComplete(bitmap: Bitmap)
        fun onDownloadFailed()
    }

     class DownloadRunnable(val url: String) : Runnable {
        override fun run() {
            Log.d("DownloadRunnable", ": Running on thread ${Thread.currentThread().name}")

            handler.obtainMessage(DOWNLOAD_INITIATED_CODE).sendToTarget()

            // Simulate network delay
            Thread.sleep(500)

            val shouldFail = Random.nextDouble(2.5) < 0.5
            if (shouldFail) {
                handler.obtainMessage(DOWNLOAD_FAILED_CODE).sendToTarget()
                return
            }

            val conn = URL(url).openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            conn.doInput = true
            conn.connect()

            val inputStream = conn.inputStream

            val bitmap = BitmapFactory.decodeStream(inputStream)
            conn.disconnect()
            inputStream.close()


            handler.obtainMessage(DOWNLOAD_COMPLETE_CODE, bitmap).sendToTarget()
        }
    }
}







